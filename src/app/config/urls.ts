export const urls = {
	//baseUrl: "http://136.243.115.134:5000/api/",
	baseUrl: "http://localhost:60815/api/",
	getToken: "User/Token",
	checkEmailAvailability: "User/CheckEmailAvailability/",
	userRegistration: "User",
	organizationsList: "Organization",
	organizationTypesList: "OrganizationType",
	userNotificationsList: "Notification",
	userAccountActivation: "User/ActivateAccount/",
	searchOrganizations: "Organization/",
	getOrganization: "Organization/GetById/"
};