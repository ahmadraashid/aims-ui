export const messages = {
    USER_REGISTRATION_MESSAGE: "Your registration has been sent to other users in your organisation for approval, or if you setup a new organisation, then it has been sent to the AIMS management for approval."
}